<?php

/*
	* Os campos na qual o usuario preencherá quando criar comentario (NOT NULL)
 	Como ele vai atribuir um valor à nacao_idnacao se ele escolherá o nome do país..? Cada País ja terá seu proprio ID?
	* Qual a diferença entre $idcomentario(int(11)) e $comentario_idcomentario(varchar(45))
	* Por que data é varchar?
	* Por que Avaliacao é varchar?
	* Os dados do Controller estão diferentes do Banco...Tipo: usuario, idanuncio, comentario..! e pra que no controller o usuario
	  vai colocar o id_usuario??? $_POST["idusuario"]
	
		

*/
class ComentarioDTO {
	// NOT NULL
	private $idcomentario; // int(11)
        private $empresa;
        private $data_inicio;
        private $data_fim;
        private $comentario; 
        private $data_postagem;
        private $voto_pos;
        private $voto_neg;
        private $usuario;
        private $pais;     
        private $avaliacao;
// varchar(200)


	/*		GETTERS		*/
        function getAvaliacao() {
            return $this->avaliacao;
        }

        function setAvaliacao($avaliacao) {
            $this->avaliacao = $avaliacao;
        }

                function getIdcomentario() {
            return $this->idcomentario;
        }

        function getEmpresa() {
            return $this->empresa;
        }

        function getData_inicio() {
            return $this->data_inicio;
        }

        function getData_fim() {
            return $this->data_fim;
        }

        function getComentario() {
            return $this->comentario;
        }

        function getData_postagem() {
            return $this->data_postagem;
        }

        function getVoto_pos() {
            return $this->voto_pos;
        }

        function getVoto_neg() {
            return $this->voto_neg;
        }

        function getUsuario() {
            return $this->usuario;
        }

        function getPais() {
            return $this->pais;
        }

        function setIdcomentario($idcomentario) {
            $this->idcomentario = $idcomentario;
        }

        function setEmpresa($empresa) {
            $this->empresa = $empresa;
        }

        function setData_inicio($data_inicio) {
            $this->data_inicio = $data_inicio;
        }

        function setData_fim($data_fim) {
            $this->data_fim = $data_fim;
        }

        function setComentario($comentario) {
            $this->comentario = $comentario;
        }

        function setData_postagem($data_postagem) {
            $this->data_postagem = $data_postagem;
        }

        function setVoto_pos($voto_pos) {
            $this->voto_pos = $voto_pos;
        }

        function setVoto_neg($voto_neg) {
            $this->voto_neg = $voto_neg;
        }

        function setUsuario($usuario) {
            $this->usuario = $usuario;
        }

        function setPais($pais) {
            $this->pais = $pais;
        }



}
?>
	
