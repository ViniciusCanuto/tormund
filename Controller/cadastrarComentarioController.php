<?php

session_start();

require_once '../Dto/comentarioDTO.php'; //Linkar com o DTO
require_once '../Dao/comentarioDAO.php'; //Linkar com o DAO


$empresa = $_POST["empresa"]; // Pegar o Post do HTML e colocar em uma var
$data_inicio = $_POST["date1"];
$data_fim = $_POST["date2"];
$comentario = $_POST["comentario"];
$nacao = $_POST["pais"];
$usuario = $_SESSION["idUsuario"];

// enviar os dados para o DTO
$comentarioDTO = new ComentarioDTO();
$comentarioDTO->setEmpresa($empresa); // setar as vars
$comentarioDTO->setData_inicio($data_inicio);
$comentarioDTO->setData_fim($data_fim);
$comentarioDTO->setComentario($comentario);
$comentarioDTO->setPais($nacao);
$comentarioDTO->setUsuario($usuario);

$comentarioDAO = new ComentarioDAO();
$sucesso = $comentarioDAO->salvarComentario($comentarioDTO); //Salvar Dados

if ($sucesso) {
    echo "<script>";
    echo "window.location.href = '../View/CadastrarComentario.php';";
    echo "window.alert('Comentário cadastrado com sucesso.');";
    echo "</script>";
}

//}
?>

