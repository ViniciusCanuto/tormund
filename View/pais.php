<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">

        <title></title>

        <link href="../Css/pais_style.css" rel="stylesheet" />
        <link href="../Css/nation.css" rel="stylesheet" />
    </head>
    <body>
        <?php
        session_start();

        $idpais = $_GET["idbandeira"];
        $selectCanada = '';
        $selectEua = '';
        $selectFr = '';
        $selectUk = '';
        $selectGer = '';

        switch ($idpais) {
            case 1: $pais = "canada";
                $imagempais = "canada.png";
                $nomepais = "CANADÁ";
                $selectCanada = 'selected';
                break;
            case 2: $pais = "eua";
                $imagempais = "eua.png";
                $nomepais = "ESTADOS UNIDOS";
                $selectEua = 'selected';
                break;

            case 3: $pais = "fr";
                $imagempais = "franca.png";
                $nomepais = "FRANÇA";
                $selectFr = 'selected';
                break;

            case 4: $pais = "uk";
                $imagempais = "uk.png";
                $nomepais = "REINO UNIDO";
                $selectUk = 'selected';
                break;

            case 5: $pais = "ger";
                $imagempais = "ger.png";
                $nomepais = "ALEMANHA";
                $selectGer = 'selected';
                break;
        }
        ?>
        <header class="col-12 background_<?php echo $pais; ?>">
            <!--
            <div class="col-12 div_header_topo">
                <a href="../index.php">
                    <div class="div_header_voltar">
                        <h5 class="h5_header">Tormund</h5>
                    </div>
                </a>
            </div> -->
            <div class="col-12 header">
                <a href="../index.php"> 
                    <div class="col-2 div_header_logo"><h2 class="h2_logo">TORMUND</h2></div>
                </a>

                <?php
                if ($_SESSION) {
                    echo "<a href='../Controller/logoffController.php'>";
                    echo "<div class='col-1 div_header'><h4 class='h4_header'>Sair</h4></div>";
                    echo "</a>";
                    echo "<div class='col-3' style='float: right;'><h4 class='h4_header'> Olá, " . $_SESSION['nome'] . "</h4></div>";
                } else {
                    echo "<a href='Login.php'>";
                    echo "<div class='col-1 div_header'><h4 class='h4_header'>Logar</h4></div>";
                    echo "</a>";

                    echo "<a href='cadastrarUsuario.php'>";
                    echo "<div class='col-1 div_header'><h4 class='h4_header'>Cadastrar</h4></div>";
                    echo "</a>";
                }
                ?>
            </div>

            <div class="col-12  div_header_inside">

                <div class="col-3 div_header_bandeira">
                    <img class="img_bandeira_header " src="<?php echo '../Img/Nacao/' . $imagempais; ?>" alt="<?php echo $nomepais ?>" />
                </div>

                <div class="col-6" style="margin-top: 18px">
                    <h1 class="h1_header_pais"><?php echo $nomepais ?></h1>
                </div>

                <!-- Filtro país -->
                
                <div class="col-1 div_header_filtro">
                    <form action="../View/pais.php">
                        <select class="form-control form-control-sm" name="idbandeira" onchange="this.form.submit()">
                            <option value="1" <?php echo $selectCanada ?>>CANADÁ</option>
                            <option value="4" <?php echo $selectUk ?>>REINO UNIDO</option>
                            <option value="2" <?php echo $selectEua ?>>ESTADOS UNIDOS</option>
                            <option value="3" <?php echo $selectFr ?>>FRANÇA</option>
                            <option value="5" <?php echo $selectGer ?>>ALEMANHA</option>
                        </select>
                    </form>
                </div> 
               
            </div>

            <div class="col-12 div_header_rodape">

                <div class="col-1 div_header_filtro">
                    <form action="">
                        <select class="form-control form-control-sm" onchange="showCustomer(this.value,<?php echo $idpais ?>)">
                            <option value="0" selected> </option>
                            <option value="5">5 ESTRELAS </option>
                            <option value="4">4 ESTRELAS</option>
                            <option value="3">3 ESTRELAS</option>
                            <option value="2">2 ESTRELAS</option>
                            <option value="1">1 ESTRELAS</option>
                        </select>
                    </form>
                </div>

                <div class="col-1 div_header_filtro">
                    <form action="">
                        <select class="form-control form-control-sm" onchange="showCustomer(this.value,<?php echo $idpais ?>)">
                            <option value="0" selected> </option>
                            <option value="21">MAIS RECENTE </option>
                            <option value="22">MAIS ANTIGO</option>
                        </select>
                    </form>
                </div> 

            </div>


        </header>

        <a href="CadastrarComentario.php">
            <div class="col-12 div_cadastrar_comentario ">
                <div class="div_inside_cadastrar_comentario background_<?php echo $pais; ?>">
                    <h6 class="h6_cadastrar_comentario">Cadastrar Comentário</h6>
                </div>

            </div>
        </a>

        <div class="col-12 div_principal div_comentario" id="target_comentario">
            <?php
            require_once '../Dao/comentarioDAO.php';
            require_once '../Function/CalcularEstrela.php';

            $comentarioDAO = new ComentarioDAO();
            $comentarios = $comentarioDAO->getComentarioByNation($idpais);
            // print_r($comentarios);
            foreach ($comentarios as $c) {
                //echo $c["comentario"];
                echo "<a href='#'>";
                echo "<div class='col-5 div_comentario_inside border_$pais'>";
                echo "<div class='col-5 div_comentario_header border_$pais background_$pais'>";

                echo "<img class='img_comentario_bandeira' src='../Img/Nacao/$imagempais' alt='$nomepais'>";
                echo "<img class='img_comentario_user' src='../Img/User/" . $c['foto'] . "' alt='user'>";
                echo "<h4 class='h4_comentario'>{$c["nome"]}</h4>";
                echo "</div>";
                echo "<div class='div_comentario_content'>";
                echo "<section class='section_comentario border_$pais'>";
                echo "<h5 class='font_$pais'>{$c["comentario"]}</h5>";
                echo "</section>";
                echo "</div>";
                echo "<div class='div_comentario_star'>";
                GerarEstrela('view',$c["avaliacao"]);
                echo "</div>";
                echo "</div>";
                echo "</a>";
            }
            ?>
        </div>

        <script>
            function showCustomer(str, pais) {
                var xhttp;
                if (str == "") {
                    document.getElementById("target_comentario").innerHTML = "";
                    return;
                }
                xhttp = new XMLHttpRequest();
                xhttp.onreadystatechange = function () {
                    if (this.readyState == 4 && this.status == 200) {
                        document.getElementById("target_comentario").innerHTML = this.responseText;
                    }
                };
                xhttp.open("GET", "../Controller/Controllerpais.php?q=" + str + "&pais=" + pais, true);
                xhttp.send();
            }
        </script>

    </body>
</html>
