<?php

require_once 'Conexao/conexao.php';

class ComentarioDAO {
    
    public $pdo = null;
    
    public function __construct() {
        $this->pdo = Conexao::getInstance();
    }
    
    public function getAllComentario() {
        try {
            $sql = "SELECT a.*,c.usuario FROM comentario c
                    INNER JOIN anuncio a ON a.idanuncio = c.anuncio_idanuncio";
                   
            $stmt = $this->pdo->prepare($sql);
            $stmt->execute();
            $feedbacks = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $feedbacks;
        } catch (PDOException $exc) {
            echo $exc->getMessage();
        }
    
    }

    public function salvarComentario(ComentarioDTO $comentarioDTO) {
        try {
          
            $pdo = Conexao::getInstance();
            $sql = "INSERT INTO comentario(empresa,data_inicio,data_fim,comentario,data_postagem, nacao_idPais,Usuario_idUsuario,voto_pos,voto_neg,avaliacao)
                    Values(?,?,?,?,NOW(),?,?,0,0,0)";
            $stmt = $pdo->prepare($sql);
            $stmt->bindValue(1, $comentarioDTO->getEmpresa());
            $stmt->bindValue(2, $comentarioDTO->getData_inicio());
            $stmt->bindValue(3, $comentarioDTO->getData_fim());
            $stmt->bindValue(4, $comentarioDTO->getComentario());
            $stmt->bindValue(5, $comentarioDTO->getPais());
            $stmt->bindValue(6, $comentarioDTO->getUsuario());
            return $stmt->execute();
        } catch (PDOException $exc) {
            echo $exc->getMessage();
        }
        }
        
        public function excluirComentarioById($idfeedback) {
        try {
            $sql = "DELETE FROM comentario WHERE idcomentario = ?";
            $stmt = $this->pdo->prepare($sql);
            $stmt->bindValue(1, $idfeedback);
            $stmt->execute();
        } catch (PDOException $exc) {
            echo $exc->getMessage();
        }
    }
    
    public function excluirComentarioByFkId($anuncios_idanuncios) {
        try {
            $sql = "DELETE FROM comentario WHERE anuncio_idanuncio = ?";
            $stmt = $this->pdo->prepare($sql);
            $stmt->bindValue(1, $anuncios_idanuncios);
            $stmt->execute();
        } catch (PDOException $exc) {
            echo $exc->getMessage();
        }
    }
    
    public function getComentarioMaxByNacao($idnacao) {
        try {
            $sql = "SELECT c.*,u.* FROM comentario c
                    INNER JOIN usuario u ON u.idUsuario = c.Usuario_idUsuario
                    WHERE c.Nacao_idPais = ?
                    ORDER BY c.avaliacao DESC
                    LIMIT 1";
            $stmt = $this->pdo->prepare($sql);
            $stmt->bindValue(1, $idnacao);
            $stmt->execute();
            $comentario = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $comentario;
         
        } catch (PDOException $exc) {
            echo $exc->getMessage();
        }
    }
    
    
    
    public function getComentarioById($idcomentario) {
        try {
            $sql = "SELECT u.*,c.* FROM comentario c
                    INNER JOIN usuario u ON u.idUsuario = c.Usuario_idUsuario
                    WHERE idcomentario = ?";
            $stmt = $this->pdo->prepare($sql);
            $stmt->bindValue(1, $idcomentario);
            $stmt->execute();
            $comentario = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $comentario;
        } catch (PDOException $exc) {
            echo $exc->getMessage();
        }
    }
    
     public function getComentarioByNation($idnacao) {
        try {
            $sql = "SELECT u.*,c.* FROM comentario c
                    INNER JOIN usuario u ON u.idUsuario = c.Usuario_idUsuario
                    WHERE Nacao_idPais = ?";
            $stmt = $this->pdo->prepare($sql);
            $stmt->bindValue(1, $idnacao);
            $stmt->execute();
            $comentario = $stmt->fetchAll(PDO::FETCH_ASSOC);
            
            return $comentario;// $comentario;
        } catch (PDOException $exc) {
            echo $exc->getMessage();
        }
    }
    
    public function getComentarioByAvaliacao($avaliacao,$idnacao) {
        try {
            $sql = "SELECT u.*,c.* FROM comentario c
                    INNER JOIN usuario u ON u.idUsuario = c.Usuario_idUsuario
                    WHERE avaliacao = ? and Nacao_idPais = ?";
            $stmt = $this->pdo->prepare($sql);
            $stmt->bindValue(1, $avaliacao);
            $stmt->bindValue(2, $idnacao);
            $stmt->execute();
            $comentario = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $comentario;
        } catch (PDOException $exc) {
            echo $exc->getMessage();
        }
    }
    
    public function getComentarioByData($idnacao,$data) {
        try {
            $sql = "SELECT u.*,c.* FROM comentario c
                    INNER JOIN usuario u ON u.idUsuario = c.Usuario_idUsuario
                    WHERE Nacao_idPais = ?
                    ORDER BY data_postagem ".$data;
            $stmt = $this->pdo->prepare($sql);
            $stmt->bindValue(1, $idnacao);
            //$stmt->bindValue(2, $data);
            $stmt->execute();
            $comentario = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $comentario;
        } catch (PDOException $exc) {
            echo $exc->getMessage();
        }
    }

     public function updateAvaliacaoById(ComentarioDTO $comentarioDTO) {
      try {
      $sql = "UPDATE comentario SET avaliacao = ? WHERE idcomentario = ?";
      $stmt = $this->pdo->prepare($sql);
      $stmt->bindValue(1, $comentarioDTO->getAvaliacao());
      $stmt->bindValue(2, $comentarioDTO->getIdcomentario());
      $stmt->execute();
      } catch (PDOException $exc) {
      echo $exc->getMessage();
      }
      } 
}
?>