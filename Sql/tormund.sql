-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 09-Jun-2019 às 21:12
-- Versão do servidor: 10.1.8-MariaDB
-- PHP Version: 5.6.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tormund`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `comentario`
--

CREATE TABLE `comentario` (
  `idComentario` int(11) NOT NULL,
  `empresa` varchar(45) NOT NULL,
  `data_inicio` date NOT NULL,
  `data_fim` date NOT NULL,
  `comentario` varchar(250) NOT NULL,
  `data_postagem` date NOT NULL,
  `Nacao_idPais` int(11) NOT NULL,
  `Usuario_idUsuario` int(11) NOT NULL,
  `voto_pos` int(11) NOT NULL,
  `voto_neg` int(11) NOT NULL,
  `avaliacao` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `comentario`
--

INSERT INTO `comentario` (`idComentario`, `empresa`, `data_inicio`, `data_fim`, `comentario`, `data_postagem`, `Nacao_idPais`, `Usuario_idUsuario`, `voto_pos`, `voto_neg`, `avaliacao`) VALUES
(1, 'CVC', '2019-05-01', '2019-05-16', 'A viagem foi muito boa', '2019-05-22', 1, 2, 10, 0, 5),
(2, 'csff', '2019-05-15', '2019-05-17', 'shgjsfgjs', '2019-05-03', 1, 2, 10, 5, 0),
(3, 'csff', '2019-05-15', '2019-05-17', 'shgjsfgjs', '2019-05-09', 1, 2, 10, 3, 0),
(4, 'cvc', '2019-06-29', '2019-06-03', '09/06/2019 uhu', '2019-06-09', 2, 5, 0, 0, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `nacao`
--

CREATE TABLE `nacao` (
  `idPais` int(11) NOT NULL,
  `nome` varchar(45) NOT NULL,
  `foto_nacao` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `nacao`
--

INSERT INTO `nacao` (`idPais`, `nome`, `foto_nacao`) VALUES
(1, 'Canada', 'canada.png'),
(2, 'Estados Unidos', 'eua.png'),
(3, 'França', 'franca.png'),
(4, 'Reino Unido', 'uk.png'),
(5, 'Alemanha', 'ger.png');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario`
--

CREATE TABLE `usuario` (
  `idUsuario` int(11) NOT NULL,
  `nome` varchar(45) NOT NULL,
  `foto` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `senha` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `usuario`
--

INSERT INTO `usuario` (`idUsuario`, `nome`, `foto`, `email`, `senha`) VALUES
(1, 'João', 'foto', 'joao@gmail.com', '123'),
(2, 'Homer Simpson', 'homer.png', 'homer@gmail.com', '123'),
(3, 'Marge Simpson', 'marge.png', 'marge@gmail.com', '123'),
(4, 'teste', 'marge.png', 'teste@gmail.com', '202cb962ac59075b964b07152d234b70'),
(5, 'VinÃ­cius Canuto', 'homer.png', 'vinicius@gmail.com', '202cb962ac59075b964b07152d234b70'),
(6, 'Vinicius Canuto', 'print 1 so.png', 'vinicius@gmail.com', '202cb962ac59075b964b07152d234b70');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `comentario`
--
ALTER TABLE `comentario`
  ADD PRIMARY KEY (`idComentario`,`Nacao_idPais`,`Usuario_idUsuario`),
  ADD KEY `fk_Comentario_Nacao_idx` (`Nacao_idPais`),
  ADD KEY `fk_Comentario_Usuario1_idx` (`Usuario_idUsuario`);

--
-- Indexes for table `nacao`
--
ALTER TABLE `nacao`
  ADD PRIMARY KEY (`idPais`);

--
-- Indexes for table `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`idUsuario`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `comentario`
--
ALTER TABLE `comentario`
  MODIFY `idComentario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `usuario`
--
ALTER TABLE `usuario`
  MODIFY `idUsuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `comentario`
--
ALTER TABLE `comentario`
  ADD CONSTRAINT `fk_Comentario_Nacao` FOREIGN KEY (`Nacao_idPais`) REFERENCES `nacao` (`idPais`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Comentario_Usuario1` FOREIGN KEY (`Usuario_idUsuario`) REFERENCES `usuario` (`idUsuario`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
