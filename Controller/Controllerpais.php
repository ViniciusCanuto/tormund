<?php

require_once '../Dao/comentarioDAO.php';

$idpais = $_GET['pais'];
$value = $_GET['q'];

$comentarioDAO = new ComentarioDAO();

switch ($value) {
    case 1:
    case 2:
    case 3:
    case 4:
    case 5:
        $comentarios = $comentarioDAO->getComentarioByAvaliacao($value, $idpais);
        break;
    case 21:
        $comentarios = $comentarioDAO->getComentarioByData($idpais, "DESC");
        break;
    case 22:
        $comentarios = $comentarioDAO->getComentarioByData($idpais, "ASC");
        break;
    case 0:
    default: $comentarios = $comentarioDAO->getComentarioByNation($idpais);
        break;
}

switch ($idpais) {
    case 1: $pais = "canada";
        $imagempais = "canada.png";
        $nomepais = "CANADÁ";
        break;
    case 2: $pais = "eua";
        $imagempais = "eua.png";
        $nomepais = "ESTADOS UNIDOS";
        break;

    case 3: $pais = "fr";
        $imagempais = "franca.png";
        $nomepais = "FRANÇA";
        break;

    case 4: $pais = "uk";
        $imagempais = "uk.png";
        $nomepais = "REINO UNIDO";
        break;

    case 5: $pais = "ger";
        $imagempais = "ger.png";
        $nomepais = "ALEMANHA";
        break;
}

foreach ($comentarios as $c) {
    //echo $c["comentario"];
    echo "<a href='#'>";
    echo "<div class='col-5 div_comentario_inside border_$pais'>";
    echo "<div class='col-5 div_comentario_header border_$pais background_$pais'>";

    echo "<img class='img_comentario_bandeira' src='../Img/Nacao/$imagempais' alt='$nomepais'>";
    echo "<img class='img_comentario_user' src='../Img/User/" . $c['foto'] . "' alt='user'>";
    echo "<h4 class='h4_comentario'>{$c["nome"]}</h4>";
    echo "</div>";
    echo "<div class='div_comentario_content'>";
    echo "<section class='section_comentario border_$pais'>";
    echo "<h5 class='font_$pais'>{$c["comentario"]}</h5>";
    echo "</section>";
    echo "</div>";
    echo "<div class='div_comentario_star'>";
    echo $c["avaliacao"];
    echo "</div>";
    echo "</div>";
    echo "</a>";
}
echo "</body>";

?>

