<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <link href="Css/style.css" rel="stylesheet" />
        <link href="Css/nation.css" rel="stylesheet" />
        <title></title>
    </head>
    <body>

        <?php
        session_start();
        //session_destroy();
        ?> 
        <!-- Menu -->
        <!--
        <ul>
            <li class="li_header"><a class="a_header" href="#contact">Logar</a></li>
            <li class="li_header" style="float:right"><a class="a_header" href="#about">Cadastrar</a></li>
        </ul>-->

        <header class="col-12 header">
            <a href="#"> 
                <div class="col-2 div_header_logo"><h2 class="h2_logo">TORMUND</h2></div>
            </a>

            <?php
            if ($_SESSION) {
                echo "<a href='Controller/logoffController.php'>";
                echo "<div class='col-1 div_header'><h4 class='h4_header'>Sair</h4></div>";
                echo "</a>";
                echo "<div class='col-3' style='float: right;'><h4 class='h4_header'> Olá, ".$_SESSION['nome']."</h4></div>";
            } else {
                echo "<a href='View/Login.php'>";
                echo "<div class='col-1 div_header'><h4 class='h4_header'>Logar</h4></div>";
                echo "</a>";
                
                echo "<a href='View/cadastrarUsuario.php'>";
                echo "<div class='col-1 div_header'><h4 class='h4_header'>Cadastrar</h4></div>";
                echo "</a>";
            }
            ?>

        </header>

        <!--Bandeiras-->
        <div class="col-12 div_principal">
            <h1>PAÍS</h1>
        </div>

        <div class="col-12 div_principal div_principal_bandeira">

            <!-- ESTADOS UNIDOS -->
            <a href="View/pais.php?idbandeira=2">
                <div class="col-2 div_bandeira border_eua" >
                    <div class="div_bandeira_inside">
                        <img class="img_bandeira" src="Img/Nacao/eua.png" alt="ESTADOS UNIDOS" />
                    </div>
                    <div class="div_bandeira_inside">

                        <h4 class="h4_bandeira font_eua">ESTADOS UNIDOS</h4>
                        <br/>
                        <h5 class="h5_bandeira font_eua">POPULAÇÃO: 327,2 milhões </h5>
                        <h5 class="h5_bandeira font_eua">CAPITAL: Washington D.C</h5>
                        <h5 class="h5_bandeira font_eua">IDIOMA: Inglês</h5>
                        <h5 class="h5_bandeira font_eua">MOEDA: Dólar Americano</h5>

                    </div>
                </div>
            </a>

            <!-- CANADA -->
            <a href="View/pais.php?idbandeira=1">
                <div class="col-2 div_bandeira border_canada" >
                    <div class="div_bandeira_inside">
                        <img class="img_bandeira" src="Img/Nacao/canada.png" alt="CANADÁ" />
                    </div>
                    <div class="div_bandeira_inside">

                        <h4 class="h4_bandeira font_canada">CANADÁ</h4>
                        <br/>
                        <h5 class="h5_bandeira font_canada">POPULAÇÃO: 37,06 milhões </h5>
                        <h5 class="h5_bandeira font_canada">CAPITAL: Ottawa</h5>
                        <h5 class="h5_bandeira font_canada">IDIOMA: Francês e Inglês</h5>
                        <h5 class="h5_bandeira font_canada">MOEDA: Dólar Canadense</h5>

                    </div>
                </div>
            </a>

            <!-- REINO UNIDO -->
            <a href="View/pais.php?idbandeira=4">
                <div class="col-2 div_bandeira border_uk" >
                    <div class="div_bandeira_inside">
                        <img class="img_bandeira" src="Img/Nacao/uk.png" style="height: 100%" alt="REINO UNIDO" />
                    </div>
                    <div class="div_bandeira_inside">

                        <h4 class="h4_bandeira font_uk">REINO UNIDO</h4>
                        <br/>
                        <h5 class="h5_bandeira font_uk">POPULAÇÃO: 66,04 milhões </h5>
                        <h5 class="h5_bandeira font_uk">CAPITAL: Londres</h5>
                        <h5 class="h5_bandeira font_uk">IDIOMA: Inglês</h5>
                        <h5 class="h5_bandeira font_uk">MOEDA: Libra Esterlina</h5>

                    </div>
                </div>
            </a>

            <!-- ALEMANHA -->
            <a href="View/pais.php?idbandeira=5">
                <div class="col-2 div_bandeira border_ger" >
                    <div class="div_bandeira_inside">
                        <img class="img_bandeira" src="Img/Nacao/ger.png" alt="ALEMANHA" />
                    </div>
                    <div class="div_bandeira_inside div_ocultar">

                        <h4 class="h4_bandeira font_ger">ALEMANHA</h4>
                        <br/>
                        <h5 class="h5_bandeira font_ger">POPULAÇÃO: 82,8 milhões</h5>
                        <h5 class="h5_bandeira font_ger">CAPITAL: Berlim</h5>
                        <h5 class="h5_bandeira font_ger">IDIOMA: Alemão</h5>
                        <h5 class="h5_bandeira font_ger">MOEDA: Euro</h5>

                    </div>
                </div>
            </a>

            <!-- FRANÇA -->
            <a href="View/pais.php?idbandeira=3">
                <div class="col-2 div_bandeira border_fr" >
                    <div class="div_bandeira_inside">
                        <img class="img_bandeira" src="Img/Nacao/franca.png" alt="FRANÇA" />
                    </div>
                    <div class="div_bandeira_inside">

                        <h4 class="h4_bandeira font_fr">FRANÇA</h4>
                        <br/>
                        <h5 class="h5_bandeira font_fr">POPULAÇÃO: 67,348 milhões </h5>
                        <h5 class="h5_bandeira font_fr">CAPITAL: Paris</h5>
                        <h5 class="h5_bandeira font_fr">IDIOMA: Francês</h5>
                        <h5 class="h5_bandeira font_fr">MOEDA: Euro e Franco</h5>

                    </div>
                </div>
            </a>
        </div>

        <!-- Melhores Comentários -->
        <div class="col-12 div_principal">
            <h1>MELHORES COMENTÁRIOS</h1>
        </div>
        <div class="col-12 div_principal div_comentario">

            <?php
            require_once './Dao/comentarioDAO.php';
            require_once './Function/CalcularEstrela.php';

            $comentarioDAO = new ComentarioDAO();
            $Canada = $comentarioDAO->getComentarioMaxByNacao(1); //Canadá


            $Eua = $comentarioDAO->getComentarioMaxByNacao(2);

            $Franca = $comentarioDAO->getComentarioMaxByNacao(3); //França
            $ReinoUnido = $comentarioDAO->getComentarioMaxByNacao(4); //uk
            $Alemanha = $comentarioDAO->getComentarioMaxByNacao(5); //alemanha
            //$idcomentarioCanda = $comentarioDAO ->getComentarioById($idcomentarioMaxCanada);
            ?>

            <a href="#">
                <div class="col-5 div_comentario_inside border_canada">
                    <div class="col-5 div_comentario_header border_canada background_canada">
                        <img class="img_comentario_bandeira" src="Img/Nacao/canada.png" alt="estados unidos">
                        <img class="img_comentario_user" src="Img/User/<?php echo $Canada[0]["foto"]; ?>" alt="user">
                        <h4 class="h4_comentario"><?php echo $Canada[0]["nome"]; ?></h4>
                    </div>
                    <div class="div_comentario_content">
                        <section class="section_comentario border_canada">
                            <h5 class="font_canada"><?php echo $Canada[0]["comentario"]; ?></h5>
                        </section>                    
                    </div>
                    <div class="div_comentario_star">
                        <?php
                            GerarEstrela('index',$Canada[0]["avaliacao"]);
                        ?>
                    </div>

                </div>
            </a>

            <a href="#">
                <div class="col-5 div_comentario_inside border_eua">
                    <div class="col-5 div_comentario_header border_eua background_eua">
                        <img class="img_comentario_bandeira" src="Img/Nacao/eua.png" alt="eua">
                        <img class="img_comentario_user" src="Img/User/<?php echo $Eua[0]["foto"]; ?>" alt="user">
                        <h4 class="h4_comentario"><?php echo $Eua[0]["nome"]; ?></h4>
                    </div>
                    <div class="div_comentario_content">
                        <section class="section_comentario border_eua">
                            <h5 class="font_eua"><?php echo $Eua[0]["foto"]; ?></h5>
                        </section>                    
                    </div>
                    <div class="div_comentario_star">
                        <?php
                            GerarEstrela('index',$Eua[0]["avaliacao"]);
                        ?>
                    </div>
                </div>
            </a>

            <a href="#">
                <div class="col-5 div_comentario_inside border_uk">
                    <div class="col-5 div_comentario_header border_uk background_uk">
                        <img class="img_comentario_bandeira" src="Img/Nacao/uk.png" alt="uk">
                        <img class="img_comentario_user" src="Img/User/<?php echo $ReinoUnido[0]["foto"]; ?>" alt="user">
                        <h4 class="h4_comentario"><?php echo $ReinoUnido[0]["nome"]; ?></h4>
                    </div>
                    <div class="div_comentario_content">
                        <section class="section_comentario border_uk">
                            <h5 class="font_uk"><?php echo $ReinoUnido[0]["comentario"]; ?></h5>
                        </section>                    
                    </div>
                    <div class="div_comentario_star">
                        <?php
                            GerarEstrela('index',$ReinoUnido[0]["avaliacao"]);
                        ?>
                    </div>
                </div>
            </a>

            <a href="#">
                <div class="col-5 div_comentario_inside border_ger">
                    <div class="col-5 div_comentario_header border_ger background_ger">
                        <img class="img_comentario_bandeira" src="Img/Nacao/ger.png" alt="Alemanha">
                        <img class="img_comentario_user" src="Img/User/<?php echo $Alemanha[0]["foto"]; ?>" alt="user">
                        <h4 class="h4_comentario"><?php echo $Alemanha[0]["foto"]; ?></h4>
                    </div>
                    <div class="div_comentario_content">
                        <section class="section_comentario border_ger">
                            <h5 class="font_ger"><?php echo $Alemanha[0]["comentario"]; ?></h5>
                        </section>                    
                    </div>
                    <div class="div_comentario_star">
                        <?php
                            GerarEstrela('index',$Alemanha[0]["avaliacao"]);
                        ?>
                    </div>
                </div>
            </a>

            <a href="#">
                <div class="col-5 div_comentario_inside border_fr">
                    <div class="col-5 div_comentario_header border_fr background_fr">
                        <img class="img_comentario_bandeira" src="Img/Nacao/franca.png" alt="fr">
                        <img class="img_comentario_user" src="Img/User/<?php echo $Franca[0]["foto"]; ?>" alt="user">
                        <h4 class="h4_comentario"><?php echo $Franca[0]["nome"]; ?></h4>
                    </div>
                    <div class="div_comentario_content">
                        <section class="section_comentario border_fr">
                            <h5 class="font_fr"><?php echo $Franca[0]["comentario"]; ?></h5>
                        </section>                    
                    </div>
                    <div class="div_comentario_star">
                       <?php
                            GerarEstrela('index',$Franca[0]["avaliacao"]);
                        ?>
                    </div>
                </div>
            </a>

        </div>


    </body>
</html>
