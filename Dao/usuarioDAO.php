<?php

require_once 'Conexao/conexao.php';

class UsuarioDAO {

    public $pdo = null;

    public function __construct() {
        $this->pdo = Conexao::getInstance();
    }
    /*
    public function getAllUsuario() {
        try {
            $sql = "SELECT u.*,p.perfilcol FROM  usuario u
                    INNER JOIN perfil p ON p.idperfil = u.perfil_idperfil";
            $stmt = $this->pdo->prepare($sql);
            $stmt->execute();
            $usuarios = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $usuarios;
        } catch (PDOException $exc) {
            echo $exc->getMessage();
        }
    }*/

    public function salvarUsuario(UsuarioDTO $usuarioDTO) {
        try {
            $sql = "INSERT INTO usuario (nome,email,senha,foto)
                    VALUES(?,?,?,?)";
            $stmt = $this->pdo->prepare($sql);
            $stmt->bindValue(1, $usuarioDTO->getNome());
            $stmt->bindValue(2, $usuarioDTO->getEmail());
            $stmt->bindValue(3, $usuarioDTO->getSenha());
            $stmt->bindValue(4, $usuarioDTO->getFoto());
            return $stmt->execute();
        } catch (PDOException $exc) {
            echo $exc->getMessage();
        }
    }    
    
}

?>
