<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Cadastrar Usuario</title>
        <link href="../Css/cadastrar_usuario.css" rel="stylesheet">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    </head>

    <body>
        <header>
            <div class="div_header_topo">
                <a href="../index.php">
                    <h1>Tormund</h1>
                </a>
            </div>
        </header>
        <!--  Comentado!!!

        <form action="../Controller/cadastrarUsuarioController.php" method="post" enctype="multipart/form-dara">

        -->

        <form action="../Controller/cadastrarUsuarioController.php" method="post" enctype="multipart/form-data">

            <!-- Div que vai agrupar todos os campos neste bloco-->
            <div class="TodosCampos">



                <!-- Foto de Perfil (Circulo) -->
                <div class="Foto">
                    <img style="width:110px; height:110px; border-radius:50%;">
                </div>

                <!-- Campo Foto de Perfil-->
                <div class="LinkFoto">
                    Foto Perfil:
                    <input type="file" name="foto" id="foto" placeholder="Nome" onchange="previewFoto()"/>

                    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
                    <script>
                        function previewFoto() {
                            var foto = document.querySelector('input[name=foto]').files[0];
                            var preview = document.querySelector('img');

                            var reader = new FileReader();
                            reader.onloadend = function () {
                                preview.src = reader.result;
                            }
                            if (foto) {
                                reader.readAsDataURL(foto);
                            } else {
                                preview.src = "";
                            }
                        }
                    </script>
                </div>

                <!-- Campo de Nome -->
                <div class="Nome">
                    Nome: 
                    <input type="text" name="nome" placeholder="Nome" required/>
                </div>

                <!-- Campo Email -->
                <div class="Email">
                    Email:
                    <input type="email" name="email" placeholder="exemplo@exemplo.com" required/>
                </div>

                <!-- Campo Senha -->
                <div class="Senha">
                    Senha:
                    <input type="password" name="senha" id="password" placeholder="Senha" required/>
                </div>

                <!-- Campo Confirmar Senha -->
                <div class="ConfirmarSenha">
                    Confirmar Senha:
                    <input type="password" name="confirmarsenha" id="confirm_password" placeholder="Confirmar Senha" required/>
                </div>
                <script>
                    var password = document.getElementById("password");
                    var confirm_password = document.getElementById("confirm_password");
                    function validatePassword() {
                        if (password.value != confirm_password.value) {
                            confirm_password.setCustomValidity("Senhas Diferentes!");
                        } else {
                            confirm_password.setCustomValidity('');
                        }
                    }
                    password.onchange = validatePassword;
                    confirm_password.onkeyup = validatePassword;
                </script>

                <!-- Botão de Cadastrar-->
                <div class="Button">
                        <button type="submit" name="botao">Cadastrar</button>
                </div>

            </div>
            </form>
    </body>
</html>
