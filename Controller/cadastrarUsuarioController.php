<?php

require_once '../Dto/usuarioDTO.php';
require_once '../Dao/usuarioDAO.php';

$nome = $_POST['nome'];
$email = $_POST['email'];
$imagem = $_FILES["foto"];
if ($_POST) {
    $senha= md5($_POST["senha"]);
    $confirmar_senha = md5($_POST["confirmarsenha"]);
    if ($senha == "") {
        die("<script>alert('Senha Vazia!');</script>");
    }
    if ($senha != $confirmar_senha) {
        die("<script>alert('As senhas não são iguais!');</script>");
    }
}


// salva a imagem na pasta 
$uploaddir = $_SERVER['DOCUMENT_ROOT'] . "/Tormund/Img/User/";
$uploadfile = $uploaddir . basename($imagem['name']);
move_uploaded_file($imagem['tmp_name'], $uploadfile);


//Estou pegando nome da imagem com a extensão
$nomeImagem = $imagem['name'];

// enviar os dados para o DTO
$usuarioDTO = new UsuarioDTO();
$usuarioDTO->setNome($nome);
$usuarioDTO->setEmail($email);
$usuarioDTO->setFoto($nomeImagem);
$usuarioDTO->setSenha($senha);





$usuarioDAO =  new UsuarioDAO();
$sucesso = $usuarioDAO->salvarUsuario($usuarioDTO);// Salva no Banco
//Função salvar Usuário está no arquivo usuarioDTO


//Alerta na tela 
if ($sucesso) {
    echo "<script>";
    echo "window.location.href = '../index.php';";
    echo "window.alert('Usuário cadastrado com sucesso!');";
    echo "</script>";
}

?>
